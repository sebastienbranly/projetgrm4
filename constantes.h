// Protection contre les multi-inclusions
#ifndef DEF_CONSTANTES
#define DEF_CONSTANTES

    // Taille d'un bloc (carr�) en pixels
    #define TAILLE_BLOC         34
    // Mettre au moins 6 pour les deux suivantes valeurs
    #define NB_BLOCS_LARGEUR    12
    #define NB_BLOCS_HAUTEUR    12
    #define LARGEUR_FENETRE     TAILLE_BLOC * NB_BLOCS_LARGEUR
    #define HAUTEUR_FENETRE     TAILLE_BLOC * NB_BLOCS_HAUTEUR
    #define NB_DIRECTIONS_PERSO 4
    // Le but est que ceci ne soit pas sur la carte
    #define NOWHERE             -1

    // Simplement pour avoir un affichage plus beau
    #define DECALAGE_NUMERO_NIVEAU_X    20
    #define DECALAGE_NUMERO_NIVEAU_Y    20

    #define TAILLE_CHAINE       100

    // Les diff�rentes orientation de notre personnage
    enum {HAUT, BAS, GAUCHE, DROITE};
    // Les diff�rentes valeurs possibles pour chaque bloc du quadrillage
    enum {VIDE, MUR, CAISSE, OBJECTIF, PERSO, CAISSE_OK};
    // Les diff�rents types de sym�tries pour l'�diteur
    enum {HORIZONTALE, VERTICALE};

#endif
