#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#include "constantes.h"
#include "map_utils.h"
#include "fichier.h"

int chargerNiveau(int numeroNiveau, int niveau[][NB_BLOCS_HAUTEUR])
{
    FILE* fichier = NULL;
    char nomFichier[TAILLE_CHAINE] = "";
    // Le fichier contient une suite de chiffres qui repr�sente la valeur de chacune des cases (ex : 0 => VIDE, etc.)
    char ligneFichier[NB_BLOCS_LARGEUR * NB_BLOCS_HAUTEUR + 1] = {0};
    int i = 0, j = 0;

    // Ouverture en mode r donc read/lecture
    sprintf(nomFichier, "data/lvl/niveau%d.lvl", numeroNiveau);
    fichier = fopen(nomFichier, "r");
    if (fichier == NULL)
    {
        // Si la lecture du fichier a �chou�, c'est probablement car le fichier n'existe pas
        // Ou bien il existe mais n'a pas pu �tre ouvert
        // Dans les deux cas, et aussi bien pour l'�diteur que le mode jeu, on d�cide d'afficher une map basique
        chargerCarteBasique(niveau);

        return EXIT_SUCCESS;
    }

    // On lit la suite de chiffres et on la stocke dans ligneFichier
    fgets(ligneFichier, NB_BLOCS_LARGEUR * NB_BLOCS_HAUTEUR + 1, fichier);

    // On va mettre � jour une � une les cases de notre map gr�ce au fichier
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
        {
            switch (ligneFichier[(i * NB_BLOCS_LARGEUR) + j])
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                    // Il y a 5 cases possibles d�finies dans le fichier constantes.h
                    // En jouant avec le code ASCII on place des int qui correspondent au type de la cellule (ex : CAISSE)
                    niveau[j][i] = ligneFichier[(i * NB_BLOCS_LARGEUR) + j] - '0';
                    break;
                default:
                    break;
            }
        }
    }

    fclose(fichier);
    return EXIT_SUCCESS;
}

int sauvegarderNiveau(int numeroNiveau, int niveau[][NB_BLOCS_HAUTEUR])
{
    FILE* fichier = NULL;
    char nomFichier[TAILLE_CHAINE] = "";
    int i = 0, j = 0;

    // Ouverture en mode w donc write/�criture
    sprintf(nomFichier, "data/lvl/niveau%d.lvl", numeroNiveau);
    fichier = fopen(nomFichier, "w");
    if (fichier == NULL)
        return EXIT_FAILURE;

    // On ins�re une � une les valeurs des cellules de la map
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
        {
            fprintf(fichier, "%d", niveau[j][i]);
        }
    }

    fclose(fichier);
    return EXIT_SUCCESS;
}
