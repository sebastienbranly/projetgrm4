// Protection contre les multi-inclusions
#ifndef DEF_FICHIER
#define DEF_FICHIER

    int chargerNiveau(int numeroNiveau, int niveau[][NB_BLOCS_HAUTEUR]);
    int sauvegarderNiveau(int numeroNiveau, int niveau[][NB_BLOCS_HAUTEUR]);

#endif
