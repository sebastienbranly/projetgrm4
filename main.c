#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#include "constantes.h"
#include "jeu.h"
#include "editeur.h"

int main(int argc, char *argv[])
{
    SDL_Surface *ecran = NULL, *menu = NULL, *flecheHaut = NULL, *flecheBas = NULL, *fondNumeroNiveau = NULL;
    SDL_Surface *chiffres[10] = {NULL};
    // Permet de coller des SDL_Surface � des positions sp�cifiques
    SDL_Rect positionMenu, position;
    position.x = 0;
    position.y = 0;
    // Permet de capter les �v�nements de l'utilisateur
    SDL_Event event;

    // Bool�en pour g�rer la boucle principale du programme
    int continuer = 1;
    int i = 0;
    char nomImage[TAILLE_CHAINE];
    // Le num�ro du niveau que l'on veut �diter/jouer, ainsi que le chiffre qui servira � d�composer ce nombre
    // Le sens sert pour faire changer le num�ro du niveau
    int numeroNiveau = 1, chiffre = 0, sens = 1, indexChiffre = 0, valeurChangement = 0;

    SDL_Init(SDL_INIT_VIDEO);
    // L'ic�ne de la fen�tre doit �tre charg�e avant SDL_SetVideoMode
    SDL_WM_SetIcon(SDL_LoadBMP("data/img/caisse.bmp"), NULL);
    // Cr�ation de la fen�tre principale
    ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    // On attribue un titre � notre fen�tre
    SDL_WM_SetCaption("Sokoban Maker", NULL);

    menu = SDL_LoadBMP("data/img/menu.bmp");
    // L'�cran de menu sera coll� tout en haut � droite
    positionMenu.x = 0;
    positionMenu.y = 0;

    fondNumeroNiveau = SDL_LoadBMP("data/img/chiffres/fond.bmp");
    flecheHaut = SDL_LoadBMP("data/img/chiffres/fleche_haut.bmp");
    SDL_SetColorKey(flecheHaut, SDL_SRCCOLORKEY, SDL_MapRGB(flecheHaut->format, 255, 0, 255));
    flecheBas = SDL_LoadBMP("data/img/chiffres/fleche_bas.bmp");
    SDL_SetColorKey(flecheBas, SDL_SRCCOLORKEY, SDL_MapRGB(flecheBas->format, 255, 0, 255));
    for (i = 0 ; i < 10 ; i++)
    {
        sprintf(nomImage, "data/img/chiffres/%d.bmp", i);
        chiffres[i] = SDL_LoadBMP(nomImage);
        SDL_SetColorKey(chiffres[i], SDL_SRCCOLORKEY, SDL_MapRGB(chiffres[i]->format, 255, 0, 255));
    }

    while (continuer)
    {
        // Attente d'un �v�n�ment de la part de l'utilisateur
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                // Clic gauche de la souris
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    // On v�rifie la partie cliqu�e pour voir s'il s'agit d'un changement du num�ro de niveau
                    indexChiffre = (event.button.x - DECALAGE_NUMERO_NIVEAU_X) / chiffres[0]->w;
                    if (event.button.x >= DECALAGE_NUMERO_NIVEAU_X && indexChiffre < 3)
                    {
                        sens = 0;
                        // On v�rifie s'il a cliqu� sur l'une des fl�ches (haut ou bas) et pas sur les chiffres
                        if (event.button.y >= DECALAGE_NUMERO_NIVEAU_Y && event.button.y < DECALAGE_NUMERO_NIVEAU_Y + flecheHaut->h)
                        {
                            // Clic sur une fl�che du haut
                            sens = 1;
                        }
                        else if ((event.button.y >= (DECALAGE_NUMERO_NIVEAU_Y + flecheHaut->h + chiffres[0]->h)) && (event.button.y < (DECALAGE_NUMERO_NIVEAU_Y + flecheHaut->h + chiffres[0]->h + flecheBas->h)))
                        {
                            // Clic sur une fl�che du bas
                            sens = -1;
                        }
                        // Si on a bien cliqu� sur une fl�che
                        if (sens != 0)
                        {
                            if (indexChiffre == 0)
                                valeurChangement = sens * 100;
                            else if (indexChiffre == 1)
                                valeurChangement = sens * 10;
                            else if(indexChiffre == 2)
                                valeurChangement = sens * 1;

                            numeroNiveau += valeurChangement;
                            // V�rification des bornes
                            if (numeroNiveau > 999)
                                numeroNiveau = 999;
                            else if (numeroNiveau < 1)
                                numeroNiveau = 1;
                        }
                    }
                }
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE: // Veut arr�ter le jeu
                        continuer = 0;
                        break;
                    case SDLK_j: // Demande � jouer
                        jouer(numeroNiveau, ecran);
                        break;
                    case SDLK_e: // Demande l'�diteur de niveaux
                        editeur(numeroNiveau, ecran);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        // Effacement de l'�cran
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
        // Collage de la surface de menu sur l'�cran
        SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
        // Collage du num�ro du niveau (de 1 � 999) d�compos� en 3 chiffres
        position.x = DECALAGE_NUMERO_NIVEAU_X;
        position.y = DECALAGE_NUMERO_NIVEAU_Y;
        SDL_BlitSurface(fondNumeroNiveau, NULL, ecran, &position);
        for (i = 0 ; i < 3 ; i++)
        {
            if (i == 0)
                chiffre = numeroNiveau / 100;
            else if (i == 1)
                chiffre = (numeroNiveau / 10) % 10;
            else
                chiffre = numeroNiveau % 10;
            position.y = DECALAGE_NUMERO_NIVEAU_Y;
            SDL_BlitSurface(flecheHaut, NULL, ecran, &position);
            position.y += flecheHaut->h;
            SDL_BlitSurface(chiffres[chiffre], NULL, ecran, &position);
            position.y += chiffres[chiffre]->h;
            SDL_BlitSurface(flecheBas, NULL, ecran, &position);

            position.x += flecheBas->w;
        }
        // Mise � jour de l'�cran
        SDL_Flip(ecran);
    }

    // On lib�re nos surfaces
    SDL_FreeSurface(menu);
    SDL_FreeSurface(flecheBas);
    SDL_FreeSurface(flecheHaut);
    SDL_FreeSurface(fondNumeroNiveau);
    for (i = 0 ; i < 10 ; i++)
        SDL_FreeSurface(chiffres[i]);
    // On quitte la SDL dont on n'a plus besoin
    SDL_Quit();

    return EXIT_SUCCESS;
}
