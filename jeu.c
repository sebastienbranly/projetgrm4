#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <string.h>

#include "constantes.h"
#include "fichier.h"
#include "map_utils.h"
#include "jeu.h"

void jouer(int numeroNiveau, SDL_Surface* ecran)
{
    /*
        DEFINITION DES VARIABLES
    */

    // Autant de surfaces que d'orientations du personnage
    SDL_Surface *personnage[NB_DIRECTIONS_PERSO] = {NULL};
    // Une surface par type de bloc sur le quadrillage
    // Remarque : personnageActuel contiendra l'orientation actuelle du personnage
    SDL_Surface *mur = NULL, *caisse = NULL, *caisseOK = NULL, *objectif = NULL, *personnageActuel = NULL;
    // Les variables pour afficher les images aux bonnes coordonn�es
    SDL_Rect position, positionJoueur;
    positionJoueur.x = 0;
    positionJoueur.y = 0;
    // La variable pour g�rer les �v�nements de l'utilisateur
    SDL_Event event;

    int continuer = 1, objectifsRestants = 0, i = 0, j = 0;
    // La map de jeu qui est une grille de cellules en 2D
    int carte[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
    char chemin[TAILLE_CHAINE] = "";
    char tempo[TAILLE_CHAINE] = "";

    /*
        CHARGEMENT DES IMAGES EN MEMOIRE
    */
    if (numeroNiveau % 2 == 0)
    {
        strcpy(chemin, "data/img/mario/");
    }
    else
    {
        strcpy(chemin, "data/img/autre_theme/");
    }

    // Chargement des sprites du jeu (d�cors, personnage)
    strcpy(tempo, chemin);
    mur = SDL_LoadBMP(strcat(chemin, "mur.bmp"));
    strcpy(chemin, tempo);

    strcpy(tempo, chemin);
    caisse = SDL_LoadBMP(strcat(chemin, "caisse.bmp"));
    strcpy(chemin, tempo);

    strcpy(tempo, chemin);
    caisseOK = SDL_LoadBMP(strcat(chemin, "caisse_ok.bmp"));
    strcpy(chemin, tempo);

    strcpy(tempo, chemin);
    objectif = SDL_LoadBMP(strcat(chemin, "objectif.bmp"));
    strcpy(chemin, tempo);
    SDL_SetColorKey(objectif, SDL_SRCCOLORKEY, SDL_MapRGB(objectif->format, 255, 0, 255));

    strcpy(tempo, chemin);
    personnage[BAS] = SDL_LoadBMP(strcat(chemin, "perso_bas.bmp"));
    strcpy(chemin, tempo);
    SDL_SetColorKey(personnage[BAS], SDL_SRCCOLORKEY, SDL_MapRGB(personnage[BAS]->format, 255, 0, 255));

    strcpy(tempo, chemin);
    personnage[GAUCHE] = SDL_LoadBMP(strcat(chemin, "perso_gauche.bmp"));
    strcpy(chemin, tempo);
    SDL_SetColorKey(personnage[GAUCHE], SDL_SRCCOLORKEY, SDL_MapRGB(personnage[GAUCHE]->format, 255, 0, 255));

    strcpy(tempo, chemin);
    personnage[HAUT] = SDL_LoadBMP(strcat(chemin, "perso_haut.bmp"));
    strcpy(chemin, tempo);
    SDL_SetColorKey(personnage[HAUT], SDL_SRCCOLORKEY, SDL_MapRGB(personnage[HAUT]->format, 255, 0, 255));

    strcpy(tempo, chemin);
    personnage[DROITE] = SDL_LoadBMP(strcat(chemin, "perso_droite.bmp"));
    strcpy(chemin, tempo);
    SDL_SetColorKey(personnage[DROITE], SDL_SRCCOLORKEY, SDL_MapRGB(personnage[DROITE]->format, 255, 0, 255));

    /*
        CHARGEMENT DU NIVEAU
    */
    chargerNiveauEtInitialiserJeu(numeroNiveau, carte, &personnageActuel, personnage, &positionJoueur);

    // Activation de la r�p�tition des touches
    SDL_EnableKeyRepeat(100, 100);

    /*
        BOUCLE PRINCIPALE DE JEU
    */
    while (continuer)
    {
        // Attente d'un �v�n�ment de la part de l'utilisateur
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                        break;
                    case SDLK_UP:
                        personnageActuel = personnage[HAUT];
                        deplacerJoueur(carte, &positionJoueur, HAUT);
                        break;
                    case SDLK_DOWN:
                        personnageActuel = personnage[BAS];
                        deplacerJoueur(carte, &positionJoueur, BAS);
                        break;
                    case SDLK_RIGHT:
                        personnageActuel = personnage[DROITE];
                        deplacerJoueur(carte, &positionJoueur, DROITE);
                        break;
                    case SDLK_LEFT:
                        personnageActuel = personnage[GAUCHE];
                        deplacerJoueur(carte, &positionJoueur, GAUCHE);
                        break;
                    case SDLK_r:
                        chargerNiveauEtInitialiserJeu(numeroNiveau, carte, &personnageActuel, personnage, &positionJoueur);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        /*
            MISE A JOUR DE L'AFFICHAGE AU SEIN DE LA BOUCLE PRINCIPALE
        */
        // Effacement de l'�cran en mettant du blanc sur chaque pixel (rgb(255,255,255))
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
        // Placement des objets � l'�cran
        objectifsRestants = 0;

        // On parcourt toute la map
        for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
        {
            for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
            {
                // On met � jour les variables permettant de coller les images aux bonnes coordonn�es
                position.x = i * TAILLE_BLOC;
                position.y = j * TAILLE_BLOC;

                // Suivant la valeur de la cellule on va coller l'image ad�quate
                switch(carte[i][j])
                {
                    case MUR:
                        SDL_BlitSurface(mur, NULL, ecran, &position);
                        break;
                    case CAISSE:
                        SDL_BlitSurface(caisse, NULL, ecran, &position);
                        break;
                    case CAISSE_OK:
                        SDL_BlitSurface(caisseOK, NULL, ecran, &position);
                        break;
                    case OBJECTIF:
                        SDL_BlitSurface(objectif, NULL, ecran, &position);
                        // On met � jour le flag qui montre que l'on ne peut toujours pas quitter le niveau car il reste un objectif
                        objectifsRestants = 1;
                        break;
                    default:
                        break;
                }
            }
        }

        // Si on n'a trouv� aucun objectif sur la carte, c'est qu'on a gagn�
        if (!objectifsRestants)
            continuer = 0;

        // On place le joueur � la bonne position
        position.x = positionJoueur.x * TAILLE_BLOC;
        position.y = positionJoueur.y * TAILLE_BLOC;
        SDL_BlitSurface(personnageActuel, NULL, ecran, &position);

        // Mise � jour de l'�cran en prenant en compte toutes les images que nous venons de coller
        SDL_Flip(ecran);
    }

    /*
        SORTIE DE LA BOUCLE PRINCIPALE DE JEU
    */
    // D�sactivation de la r�p�tition des touches (remise � 0)
    SDL_EnableKeyRepeat(0, 0);

    // Lib�ration des surfaces charg�es
    SDL_FreeSurface(mur);
    SDL_FreeSurface(caisse);
    SDL_FreeSurface(caisseOK);
    SDL_FreeSurface(objectif);
    for (i = 0 ; i < NB_DIRECTIONS_PERSO ; i++)
        SDL_FreeSurface(personnage[i]);
}

void deplacerJoueur(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, int direction)
{
    // pos correspond � la position actuel du personnage, mais c'est un pointeur (passage par adresse)
    // Au d�part on consid�re que le mouvement est autoris�
    int dirX = 0, dirY = 0, mouvementInterdit = 0;

    // On modifie ces valeurs pour avoir le vecteur de d�placement du personnage
    // Cela permet de factoriser le code
    switch(direction)
    {
        case HAUT:
            dirY = -1;
            break;
        case BAS:
            dirY = 1;
            break;
        case GAUCHE:
            dirX = -1;
            break;
        case DROITE:
        default:
            dirX = 1;
            break;
    }

    // On v�rifie que l'on ne d�passe pas l'�cran
    if (!(valeurEntre(0, pos->x + dirX, NB_BLOCS_LARGEUR - 1) && valeurEntre(0, pos->y + dirY, NB_BLOCS_HAUTEUR - 1)))
        mouvementInterdit = 1;
    // On v�rifie qu'il n'y ait pas de mur � l'arriv�e
    if (carte[pos->x + dirX][pos->y + dirY] == MUR)
        mouvementInterdit = 1;

    // Si on veut pousser une caisse, il faut v�rifier qu'il n'y a pas de mur derri�re (ou une autre caisse, ou la limite du monde)
    if (estUneCaisse(carte[pos->x + dirX][pos->y + dirY]))
        if (!valeurEntre(0, pos->x + 2*dirX, NB_BLOCS_LARGEUR - 1) || !valeurEntre(0, pos->y + 2*dirY, NB_BLOCS_HAUTEUR - 1) || estUnBloc(carte[pos->x + 2*dirX][pos->y + 2*dirY]))
            mouvementInterdit = 1;
    // Le test pr�c�dent se r�sume sous la forme : si l'on veut pousser une caisse, on interdit le mouvement si la cellule derri�re
    // est hors bords, ou bien est une caisse ou bien est un mur

    if (!mouvementInterdit)
    {
        // Si on arrive l�, c'est qu'on peut d�placer le joueur !
        // On v�rifie d'abord s'il y a une caisse � d�placer
        deplacerCaisse(&carte[pos->x + dirX][pos->y + dirY], &carte[pos->x + 2*dirX][pos->y + 2*dirY]);

        pos->x += dirX;
        pos->y += dirY;
    }
}

void deplacerCaisse(int *premiereCase, int *secondeCase)
{
    // Il faut bien faire attention � g�rer tous les cas lors du d�placement d'une caisse
    if (estUneCaisse(*premiereCase))
    {
        if (*secondeCase == OBJECTIF)
            *secondeCase = CAISSE_OK;
        else
            *secondeCase = CAISSE;

        if (*premiereCase == CAISSE_OK)
            *premiereCase = OBJECTIF;
        else
            *premiereCase = VIDE;
    }
}

int estUneCaisse(int valeurCellule)
{
    if (valeurCellule == CAISSE || valeurCellule == CAISSE_OK)
        return 1;
    return 0;
}

int estUnBloc(int valeurCellule)
{
    if (valeurCellule == MUR)
        return 1;
    return estUneCaisse(valeurCellule);
}

int valeurEntre(int mini, int valeur, int maxi)
{
    return (valeur >= mini && valeur <= maxi) ? 1 : 0;
}

void chargerNiveauEtInitialiserJeu(const int numeroNiveau, int carte[][NB_BLOCS_HAUTEUR], SDL_Surface ** personnageActuel, SDL_Surface * personnage[NB_DIRECTIONS_PERSO], SDL_Rect * positionJoueur)
{
    int i = 0, j = 0;

    if (chargerNiveau(numeroNiveau, carte) == EXIT_FAILURE)
        exit(EXIT_FAILURE);

    /*
        INITIALISATIONS
    */
    // Au d�but le personnage sera orient� vers le bas
    *personnageActuel = personnage[BAS];

    // Recherche de la position du personnage au d�part
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
        {
            if (carte[i][j] == PERSO)
            {
                // On stocke la position du personnage sur la map et affecte vide � la case
                positionJoueur->x = i;
                positionJoueur->y = j;
                carte[i][j] = VIDE;
            }
        }
    }
}
