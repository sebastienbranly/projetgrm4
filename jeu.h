// Protection contre les multi-inclusions
#ifndef DEF_JEU
#define DEF_JEU

    void jouer(int numeroNiveau, SDL_Surface* ecran);
    void deplacerJoueur(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, int direction);
    void deplacerCaisse(int *premiereCase, int *secondeCase);
    int estUneCaisse(int valeurCellule);
    int estUnBloc(int valeurCellule);
    int valeurEntre(int mini, int valeur, int maxi);
    void chargerNiveauEtInitialiserJeu(const int numeroNiveau, int carte[][NB_BLOCS_HAUTEUR], SDL_Surface ** personnageActuel, SDL_Surface * personnage[NB_DIRECTIONS_PERSO], SDL_Rect * positionJoueur);

#endif
