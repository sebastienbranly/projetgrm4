// Protection contre les multi-inclusions
#ifndef DEF_EDITEUR
#define DEF_EDITEUR

    void editeur(int numeroNiveau, SDL_Surface* ecran);
    void supprimerPerso(int carte[][NB_BLOCS_HAUTEUR]);
    int carteValide(int carte[][NB_BLOCS_HAUTEUR]);
    void symetrieCarte(int carte[][NB_BLOCS_HAUTEUR], int typeSymetrie);
    void miniEtMaxi(int valeur1, int valeur2, int * mini, int * maxi);

#endif
