// Protection contre les multi-inclusions
#ifndef DEF_MAP_UTILS
#define DEF_MAP_UTILS

    // Fonctions qui peuvent servir strictement aussi bien au jeu qu'� l'�diteur
    int nombreDe(int valeur, int carte[][NB_BLOCS_HAUTEUR]);
    void chargerCarteBasique(int carte[][NB_BLOCS_HAUTEUR]);

#endif
