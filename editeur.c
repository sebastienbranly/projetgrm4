#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#include "constantes.h"
#include "fichier.h"
#include "map_utils.h"
#include "editeur.h"

void editeur(int numeroNiveau, SDL_Surface* ecran)
{
    SDL_Surface *mur = NULL, *caisse = NULL, *objectif = NULL, *personnage = NULL, *caisse_ok = NULL, *curseur = NULL, *curseurDebutRectangle = NULL;
    SDL_Rect position, positionSouris;
    positionSouris.x = 0;
    positionSouris.y = 0;
    SDL_Event event;

    // clicGauche et Droit EnCours permettent de pouvoir ajouter plusieurs �l�ments en restant cliqu� et bougeant la souris
    int continuer = 1, clicGaucheEnCours = 0, clicDroitEnCours = 0;
    int objetActuel = MUR, i = 0, j = 0, modeRectangleActive = 0, estPremierClicRectangle = 0, clicDebutRectangleX = NOWHERE, clicDebutRectangleY = NOWHERE, miniX, maxiX, miniY, maxiY;
    int carte[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};

    // Chargement des objets et du niveau
    mur = SDL_LoadBMP("data/img/mur.bmp");
    caisse = SDL_LoadBMP("data/img/caisse.bmp");
    objectif = SDL_LoadBMP("data/img/objectif.bmp");
    SDL_SetColorKey(objectif, SDL_SRCCOLORKEY, SDL_MapRGB(objectif->format, 255, 0, 255));
    personnage = SDL_LoadBMP("data/img/mario_bas.bmp");
    SDL_SetColorKey(personnage, SDL_SRCCOLORKEY, SDL_MapRGB(personnage->format, 255, 0, 255));
    caisse_ok = SDL_LoadBMP("data/img/caisse_ok.bmp");
    curseur = SDL_LoadBMP("data/img/curseur.bmp");
    SDL_SetColorKey(curseur, SDL_SRCCOLORKEY, SDL_MapRGB(curseur->format, 255, 0, 255));
    curseurDebutRectangle = SDL_LoadBMP("data/img/curseurDebutRectangle.bmp");
    SDL_SetColorKey(curseurDebutRectangle, SDL_SRCCOLORKEY, SDL_MapRGB(curseurDebutRectangle->format, 255, 0, 255));

    // Chargement du niveau que l'on veut �diter
    if (chargerNiveau(numeroNiveau, carte) == EXIT_FAILURE)
        exit(EXIT_FAILURE);

    /*
        BOUCLE PRINCIPALE DE L'EDITEUR
    */
    while (continuer)
    {
        // Attente d'un �v�n�ment de la part de l'utilisateur
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                // On n'autorise � quitter que si la map est valide
                if (carteValide(carte))
                    continuer = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                // Clic de la souris, gauche ou bien droit
                if (modeRectangleActive)
                {
                    if (estPremierClicRectangle)
                    {
                        // C'est le premier clic pour la fonction rectangle
                        clicDebutRectangleX = event.button.x / TAILLE_BLOC;
                        clicDebutRectangleY = event.button.y / TAILLE_BLOC;
                    }
                    else
                    {
                        miniEtMaxi(clicDebutRectangleX, event.button.x / TAILLE_BLOC, &miniX, &maxiX);
                        miniEtMaxi(clicDebutRectangleY, event.button.y / TAILLE_BLOC, &miniY, &maxiY);

                        for (i = miniX ; i <= maxiX ; i++)
                        {
                            for (j = miniY ; j <= maxiY ; j++)
                            {
                                if (event.button.button == SDL_BUTTON_LEFT)
                                    carte[i][j] = objetActuel;
                                else if (event.button.button == SDL_BUTTON_RIGHT)
                                    carte[i][j] = VIDE;
                            }
                        }

                        // R�initialisation des param�tres pour le prochain rectangle
                        clicDebutRectangleX = NOWHERE;
                        clicDebutRectangleY = NOWHERE;
                    }
                    estPremierClicRectangle = 1 - estPremierClicRectangle;
                }
                else if (event.button.button == SDL_BUTTON_LEFT)
                {
                    // On met l'objet actuellement choisi (mur, caisse...) � l'endroit du clic
                    // Si l'objet actuel � placer est le personnage, on supprime toute pr�sence du personnage ailleurs
                    if (objetActuel == PERSO)
                        supprimerPerso(carte);
                    carte[event.button.x / TAILLE_BLOC][event.button.y / TAILLE_BLOC] = objetActuel;
                    clicGaucheEnCours = 1; // On retient qu'un bouton est enfonc�
                }
                else if (event.button.button == SDL_BUTTON_RIGHT)
                {
                    carte[event.button.x / TAILLE_BLOC][event.button.y / TAILLE_BLOC] = VIDE;
                    clicDroitEnCours = 1;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                // Le doigt est retir� de la souris ("up")
                if (!modeRectangleActive)
                {
                    // On d�sactive le bool�en qui disait qu'un bouton �tait enfonc�
                    if (event.button.button == SDL_BUTTON_LEFT)
                        clicGaucheEnCours = 0;
                    else if (event.button.button == SDL_BUTTON_RIGHT)
                        clicDroitEnCours = 0;
                }
                break;
            case SDL_MOUSEMOTION:
                // Mouvement de la souris en cours
                positionSouris.x = event.motion.x;
                positionSouris.y = event.motion.y;
                if (!modeRectangleActive)
                {
                    if (clicGaucheEnCours) // Si on d�place la souris et que le bouton gauche de la souris est enfonc�
                    {
                        // Si l'objet actuel � placer est le personnage, on supprime toute pr�sence du personnage ailleurs
                        if (objetActuel == PERSO)
                            supprimerPerso(carte);
                        carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = objetActuel;
                    }
                    else if (clicDroitEnCours) // Pareil pour le bouton droit de la souris
                    {
                        carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = VIDE;
                    }
                }
                break;
            case SDL_KEYDOWN:
                // Appui sur une touche du clavier
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        // On n'autorise � quitter que si la map est valide
                        if (carteValide(carte))
                            continuer = 0;
                        break;
                    case SDLK_UP:
                        if (!modeRectangleActive)
                        {
                            // On change l'objet de l'�diteur en g�rant les bords
                            objetActuel++;
                            if (objetActuel > CAISSE_OK)
                                objetActuel = MUR;
                        }
                        break;
                    case SDLK_DOWN:
                        if (!modeRectangleActive)
                        {
                            // On change l'objet de l'�diteur en g�rant les bords
                            objetActuel--;
                            if (objetActuel < MUR)
                                objetActuel = CAISSE_OK;
                        }
                        break;
                    case SDLK_f:
                        symetrieCarte(carte, HORIZONTALE);
                        break;
                    case SDLK_g:
                        symetrieCarte(carte, VERTICALE);
                        break;
                    case SDLK_r:
                        // Mode rectangle
                        if (objetActuel != PERSO)
                        {
                            // Switch du booleen
                            modeRectangleActive = 1 - modeRectangleActive;
                            estPremierClicRectangle = 1;
                            clicDebutRectangleX = NOWHERE;
                            clicDebutRectangleY = NOWHERE;
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        /*
            AFFICHAGE AU SEIN DE LA BOUCLE PRINCIPALE DE L'EDITEUR
        */
        // Effacement de l'�cran
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

        // Placement des objets � l'�cran
        for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
        {
            for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
            {
                position.x = i * TAILLE_BLOC;
                position.y = j * TAILLE_BLOC;

                switch(carte[i][j])
                {
                    case MUR:
                        SDL_BlitSurface(mur, NULL, ecran, &position);
                        break;
                    case CAISSE:
                        SDL_BlitSurface(caisse, NULL, ecran, &position);
                        break;
                    case OBJECTIF:
                        SDL_BlitSurface(objectif, NULL, ecran, &position);
                        break;
                    case PERSO:
                        SDL_BlitSurface(personnage, NULL, ecran, &position);
                        break;
                    case CAISSE_OK:
                        SDL_BlitSurface(caisse_ok, NULL, ecran, &position);
                        break;
                    default:
                        break;
                }
            }
        }

        /*
            AFFICHAGE D'UN CURSEUR
        */
        // Calcul de la case point�e par la souris
        i = positionSouris.x / TAILLE_BLOC;
        j = positionSouris.y / TAILLE_BLOC;
        // Calcul de la position o� blitter le curseur (�a arrondit gr�ce � la propri�t� des int en C)
        position.x = i * TAILLE_BLOC;
        position.y = j * TAILLE_BLOC;
        if (modeRectangleActive)
        {
            SDL_BlitSurface(curseurDebutRectangle, NULL, ecran, &position);
            if (clicDebutRectangleX != NOWHERE && clicDebutRectangleY != NOWHERE)
            {
                position.x = clicDebutRectangleX * TAILLE_BLOC;
                position.y = clicDebutRectangleY * TAILLE_BLOC;
                SDL_BlitSurface(curseurDebutRectangle, NULL, ecran, &position);
            }
        }
        else
            SDL_BlitSurface(curseur, NULL, ecran, &position);

        // Affichage de l'object actuel qui suit la position de la souris
        switch(objetActuel)
        {
            case MUR:
                SDL_SetAlpha(mur, SDL_SRCALPHA, 128);
                SDL_BlitSurface(mur, NULL, ecran, &positionSouris);
                SDL_SetAlpha(mur, SDL_SRCALPHA, 255);
                break;
            case CAISSE:
                SDL_SetAlpha(caisse, SDL_SRCALPHA, 128);
                SDL_BlitSurface(caisse, NULL, ecran, &positionSouris);
                SDL_SetAlpha(caisse, SDL_SRCALPHA, 255);
                break;
            case OBJECTIF:
                SDL_SetAlpha(objectif, SDL_SRCALPHA, 128);
                SDL_BlitSurface(objectif, NULL, ecran, &positionSouris);
                SDL_SetAlpha(objectif, SDL_SRCALPHA, 255);
                break;
            case PERSO:
                SDL_SetAlpha(personnage, SDL_SRCALPHA, 128);
                SDL_BlitSurface(personnage, NULL, ecran, &positionSouris);
                SDL_SetAlpha(personnage, SDL_SRCALPHA, 255);
                break;
            case CAISSE_OK:
                SDL_SetAlpha(caisse_ok, SDL_SRCALPHA, 128);
                SDL_BlitSurface(caisse_ok, NULL, ecran, &positionSouris);
                SDL_SetAlpha(caisse_ok, SDL_SRCALPHA, 255);
                break;
            default:
                break;
        }

        // Mise � jour de l'�cran
        SDL_Flip(ecran);
    }

    // A la sortie de l'�diteur on sauvegarde automatiquement le niveau cr��
    sauvegarderNiveau(numeroNiveau, carte);

    /*
        LIBERATION DE LA MEMOIRE
    */
    SDL_FreeSurface(mur);
    SDL_FreeSurface(caisse);
    SDL_FreeSurface(objectif);
    SDL_FreeSurface(personnage);
    SDL_FreeSurface(caisse_ok);
}

void supprimerPerso(int carte[][NB_BLOCS_HAUTEUR])
{
    int i = 0, j = 0;
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
        {
            if (carte[i][j] == PERSO)
                carte[i][j] = VIDE;
        }
    }
}

void symetrieCarte(int carte[][NB_BLOCS_HAUTEUR], int typeSymetrie)
{
    int i = 0, j = 0, valeurTemporaire = 0;
    switch(typeSymetrie)
    {
        case HORIZONTALE:
            for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
                for (i = 0 ; i < NB_BLOCS_LARGEUR / 2 ; i++)
                {
                    valeurTemporaire = carte[i][j];
                    carte[i][j] = carte[NB_BLOCS_LARGEUR - 1 - i][j];
                    carte[NB_BLOCS_LARGEUR - 1 - i][j] = valeurTemporaire;
                }
            break;
        case VERTICALE:
            for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
                for (j = 0 ; j < NB_BLOCS_HAUTEUR / 2 ; j++)
                {
                    valeurTemporaire = carte[i][j];
                    carte[i][j] = carte[i][NB_BLOCS_HAUTEUR - 1 - j];
                    carte[i][NB_BLOCS_HAUTEUR - 1 - j] = valeurTemporaire;
                }
            break;
        default:
            break;
    }
}

int carteValide(int carte[][NB_BLOCS_HAUTEUR])
{
    // La map doit avoir un seul personnage et autant de caisses que d'objectifs (on se moque des caisses ok qui sont d�j� bien plac�es)
    return (nombreDe(PERSO, carte) == 1 && nombreDe(OBJECTIF, carte) >= 1 && nombreDe(CAISSE, carte) >= nombreDe(OBJECTIF, carte));
}

// Range dans les variables point�es mini et maxi les bonnes valeurs
void miniEtMaxi(int valeur1, int valeur2, int * mini, int * maxi)
{
    if (valeur1 >= valeur2)
    {
        *mini = valeur2;
        *maxi = valeur1;
    }
    else
    {
        *mini = valeur1;
        *maxi = valeur2;
    }
}
