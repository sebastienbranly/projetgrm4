#include "constantes.h"
#include "map_utils.h"

int nombreDe(int valeur, int carte[][NB_BLOCS_HAUTEUR])
{
    int i = 0, j = 0, compteur = 0;
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
        {
            if (carte[i][j] == valeur)
                compteur++;
        }
    }
    return compteur;
}

void chargerCarteBasique(int carte[][NB_BLOCS_HAUTEUR])
{
    int i = 0, j = 0;
    /*
        DESSIN DES BORDS
    */
    // Bords horizontaux
    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
    {
        carte[i][0] = MUR;
        carte[i][NB_BLOCS_HAUTEUR - 1] = MUR;
    }
    // Bords verticaux
    for (j = 1 ; j < NB_BLOCS_HAUTEUR - 1 ; j++)
    {
        carte[0][j] = MUR;
        carte[NB_BLOCS_LARGEUR - 1][j] = MUR;
    }

    // Ajout d'�l�ments suffisants pour terminer le niveau par d�faut
    carte[NB_BLOCS_LARGEUR / 2 - 1][NB_BLOCS_HAUTEUR / 2] = PERSO;
    carte[NB_BLOCS_LARGEUR / 2][NB_BLOCS_HAUTEUR / 2] = CAISSE;
    carte[NB_BLOCS_LARGEUR / 2 + 1][NB_BLOCS_HAUTEUR / 2] = OBJECTIF;
}
